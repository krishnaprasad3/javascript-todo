let hidedescription = () => {
  let description = document.getElementsByClassName("des-hide")[0];
  let datablock = document.getElementById("datablock");
  datablock.style.display = "block";
  description.style.display = "none";
};
let msg = document.getElementById("msg");
let content = document.getElementById("content");
let dates = document.getElementById("todate");
let timer = document.getElementById("totime");
let tablerow = document.getElementById("tablerow");
let todo = [];

// preventDefault for loading page while submitting
// It is called synthetic events
let form = document.getElementById("form");
form.addEventListener("submit", (e) => {
  e.preventDefault();
  // console.log("button clicked");
  if (formValidation()) {
    makeUser(content, dates, timer);
    fillrow();
    reset();
    datetime();
  }
});

//while submmiting reset form
let reset = () => {
  document.getElementById("form").reset();
};
// form validation message and input

// default date  // default time
let datetime = () => {
  function padTo2Digits(num) {
    return num.toString().padStart(2, "0"); // two digit set start with zero
  }

  function formatDate(date = new Date()) {
    return (
      [
        date.getFullYear(),
        padTo2Digits(date.getMonth() + 1),
        padTo2Digits(date.getDate()),
      ].join("-") +
      " " +
      [
        padTo2Digits(date.getHours()),
        padTo2Digits(date.getMinutes()),
        // padTo2Digits(date.getSeconds()),  // can also add seconds
      ].join(":")
    );
  }

  const [date, time] = formatDate(new Date()).split(" ");
  document.getElementById("todate").value = date;

  document.getElementById("totime").value = time;
};
window.onload = datetime(); //datetime function because to get preset date and time after reset

// formValidation

let formValidation = () => {
  if (dates.value === "" && timer.value === "" && content.value === "") {
    msg.innerHTML = "please Enter your Plan to Do";
    console.log("failure");
  } else if (dates.value === "" && timer.value !== "" && content.value !== "") {
    // console.log("date not entered");
    msg.innerHTML = "please select a Date";
  } else if (dates.value !== "" && timer.value === "" && content.value !== "") {
    // console.log("time not entered");
    msg.innerHTML = "please select Time";
  } else if (dates.value !== "" && timer.value !== "" && content.value === "") {
    msg.innerHTML = "please Enter the Description";
  } else {
    msg.innerHTML = "";
    return true;
  }
};

let selectedIndex = [];

function makeUser(content, datex, timer) {
  todo.push({
    content: content.value,
    datex: new Date(datex.value).toLocaleDateString(),
    timer: timer.value,
  });
}

let checkItem = (index) => {
  if (selectedIndex.includes(index)) {
    selectedIndex = selectedIndex.filter((i) => i != index);
  } else {
    selectedIndex.push(index);
  }

  // console.log(selectedIndex.includes(0));
  fillrow();
};

let deleterow = (index) => {
  selectedIndex.push(index);
  if (index > -1) {
    // only splice array when item is found
    todo.splice(index, 1); // 2nd parameter means remove one item only
  }

  selectedIndex = [];
  fillrow();
  console.log(selectedIndex);
};

let fillrow = () => {
  let dataIn = "";

  todo.forEach((element, index) => {
    dataIn += `<tr class="border-b border-sky-900 text-sm">
                  <td class="py-3 px-6 text-center font-medium"> 
                    <input onChange="checkItem(${index})" type="checkbox" id=${index}
                      class="rounded border-gray-400 text-green-600 shadow-sm focus:border-green-300 focus:ring focus:ring-offset-0 focus:ring-green-200 focus:ring-opacity-50" />
                  </td>
                  <td class="py-4 px-6 text-center font-medium">
                    ${element.datex + ", " + element.timer}
                  </td>

                  <td
                    class="py-3 px-6 text-center font-medium bg-teal-50 decoration-red-500 ${
                      selectedIndex.includes(index) ? "line-through" : ""
                    }">
                    ${element.content}
                  </td>
                  <td class="py-3 px-6 text-center font-medium">
                    <span class="p-2 bg-lime-500 rounded-full ${
                      selectedIndex.includes(index) ? "visible" : "hidden"
                    } ">Done</span>
                    <span class="p-2 bg-red-500 rounded-full ${
                      selectedIndex.includes(index) ? "hidden" : "visible"
                    }"
                      >Not Done</span>
                  </td>
                  <td
                    class="py-5 px-6 text-center font-medium flex justify-around bg-teal-50">
                    <button  class="text-red-500" onclick="deleterow(${index})" >
                      <svg
                        class="w-5 text-red-500 fill-current w-5 h-6"
                        xmlns="http://www.w3.org/2000/svg"
                        viewBox="0 0 448 512">
                        <path
                          d="M32 464a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128H32zm272-256a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zm-96 0a16 16 0 0 1 32 0v224a16 16 0 0 1-32 0zM432 32H312l-9.4-18.7A24 24 0 0 0 281.1 0H166.8a23.72 23.72 0 0 0-21.4 13.3L136 32H16A16 16 0 0 0 0 48v32a16 16 0 0 0 16 16h416a16 16 0 0 0 16-16V48a16 16 0 0 0-16-16z" />
                      </svg>
                    </button>
                  </td>
                </tr>`;
  });

  tablerow.innerHTML = dataIn;

  selectedIndex.forEach((index) => {
    let checkbox = document.getElementById("" + index);
    checkbox.checked = true;
  });
};
